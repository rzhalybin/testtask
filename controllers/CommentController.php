<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\components;

class CommentController extends Controller
{
	private $post = array();
	public function beforeAction($action) 
	{
		$this->post = json_decode(file_get_contents("php://input"));
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionGet()
	{
		$response = array('success' => false);
		if (isset($this->post->rec_no)) {
			$connection = \Yii::$app->db;
			$command = $connection->createCommand('SELECT u.name as user_name, body as comment, c.id as comment_id, parent_id, path
FROM comments c
LEFT JOIN comments_tree ct ON c.id = ct.id
INNER JOIN users u ON u.id = c.owner_id
WHERE c.record_id = '.$this->post->rec_no.' ORDER BY LENGTH(ct.path)');
			$comments = $command->queryAll();
			$response['success'] = true;
			$response['data'] = $comments;
		}
		
		echo json_encode($response);
		Yii::$app->end();
	}
	
	public function actionRemove()
	{
		$response = array('success' => false);
		if (isset($this->post->id)) {
			$connection = \Yii::$app->db;
			$command = $connection->createCommand('SELECT path 
FROM comments_tree 
WHERE path LIKE "%'.$this->post->id.'%"');
			$paths = $command->queryAll();
			
			$all_paths = array();
			foreach ($paths as $row) {
				$all_paths[] = preg_replace('/.*('.$this->post->id.')/', '$1', $row['path']);
			}
			if ($all_paths) {
				$all_paths = implode(',', $all_paths);
				$all_paths = explode(',', $all_paths);
				$all_paths = array_unique($all_paths);
				$path = implode(',', $all_paths);
			}
			else {
				$path = $this->post->id;
			}
			\app\models\Comments::deleteAll('id IN ('.$path.')');
			\app\models\CommentsTree::deleteAll('id IN ('.$path.')');
			$response['success'] = true;
			$response['data'] = $path;
		}
		
		echo json_encode($response);
		Yii::$app->end();
	}
}