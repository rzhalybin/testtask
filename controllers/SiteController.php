<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGenerate()
    {
		$error = false;
		$start = microtime(true);
		$connection = \Yii::$app->db;
		
		$params = Yii::$app->request->get();
		if (empty($params['users']) || empty($params['records']) || empty($params['comments'])) {
			$error = 'Invalid params';
		}
		if (!$error) {
			if (empty($params['deep'])) {
				$deep = 1;
			}
			\app\models\Users::preGenerateNewUsers($params['users']);
			$new_users = \app\models\Users::getAllNewUsers();

			\app\models\Records::generateNewRecords($new_users, $params['records']);
			$new_records = \app\models\Records::getAllNewRecords($new_users);

			\app\models\Comments::generateNewComments($new_users, $new_records, $params['comments'], $params['deep']);

			\app\models\Users::finishGenerateNewUsers($new_users);
		}
		$duration = microtime(true) - $start;
        return $this->render('generate', array('duration' => $duration, 'error' => $error));
    }

    public function actionShow()
	{
		$params = Yii::$app->request->get();
		return $this->render('show', array('name' => Yii::$app->request->csrfParam, 'token' => Yii::$app->request->getCsrfToken()));
	}
	
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
