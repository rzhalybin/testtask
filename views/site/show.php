<div ng-app="displayComments" ng-controller="DisplayCommentsController as display_comments">
	<div>
		<input id="rec_no" type="text" value="{{display_comments.rec_no}}">
		<button ng-click="getComments()">Show</button>
	</div>

	<script type="text/ng-template"  id="node.html">
		<div>User: <b>{{node.value.user_name}}</b> - Comment ID: {{node.value.comment_id}}</div>{{node.value.comment}} <span style="cursor: pointer; color: red" ng-click="deleteComment(node.value.comment_id)">Delete</span>
		<ul>
			<li id="{{node.value.comment_id}}" ng-repeat="node in node.children track by $index" ng-include="'node.html'"></li>
		</ul>
	</script>
	<ul>
		<li id="{{node.value.comment_id}}" ng-repeat="node in commentList track by $index" ng-include="'node.html'"></li>
	</ul>
</div>
<script type="text/javascript" src="/js/angular.js"></script>
<script type="text/javascript">
	var rec_no     = 244;
	var token_name = '<?php echo $name;?>';
	var token_val  = '<?php echo $token; ?>';
	var app = angular.module( "displayComments", [] );
	app.controller(
		"DisplayCommentsController",
		function($scope, commentsService) {
			this.rec_no = rec_no;
			$scope.commentList = {};
			function updateComment(comment_id) {
				commentsService.updateComment(comment_id);
			}
			$scope.getComments = function () {
				rec_no = angular.element(document.querySelector('#rec_no')).val();
				var comments = commentsService.getComments(rec_no);
				comments.success(function (response) {
					var res = [];
					var index = 0;
					var map = [];
					for (var i in response.data) {
						var el = response.data[i];
						map[el.comment_id] = index;
						if (el.parent_id == 0) {
							res[map[el.comment_id]] = {value: el, children: {}};
						}
						else {
							var path = el.path.split(',');
							var path_el = res[map[path.shift()]].children;
							if (path.length > 1) {
								var t = 0;
							}
							for (var j = 0; j < path.length; j++) {
								if (path_el.hasOwnProperty(map[path[j]])) {
									path_el = path_el[map[path[j]]].children;
								}
							}
							path_el[map[el.comment_id]] = {value: el, children: {}};
						}
						index++;
					}
					if (!res.length) {
						alert('no comments');
					}
					$scope.commentList = res;
				});
			}
			
			$scope.deleteComment = function (id) {
				var comments = commentsService.removeComment(id);
				comments.success(function (response) {
					var ids = response.data.split(',');
					for (var i in ids) {
						angular.element($('#'+ids[i])).remove();
					}
				});
			}
		}
	);
	
	app.service(
		"commentsService",
		function ($http, $q) {
			
			var ajax_url = 'index.php?r=comment%2F';
//			var ajax_url = 'index.php';

			// Return public API.
			return({
				updateComment: updateComment,
				removeComment: removeComment,
				getComments: getComments
			});


			// ---
			// PUBLIC METHODS.
			// ---


			// I add a friend with the given name to the remote collection.
			function updateComment(id) {
//				var params = {
//					r: "comment/update",
//				};
//				params[token_name] = token_val;
//				var request = $http({
//					method: "post",
//					url: ajax_url,
//					params: params,
//					data: {
//						rec_no: id
//					}
//				});
				var request = $http.post(ajax_url+'update', {comment_id: id});
				return( request.then( handleSuccess, handleError ) );
			}


			// I get all of the friends in the remote collection.
			function getComments(rec_no) {
				return $http.post(ajax_url+'get', {rec_no: rec_no});
			}


			// I remove the friend with the given ID from the remote collection.
			function removeComment( id ) {
				return $http.post(ajax_url+'remove', {id: id});
			}


			// ---
			// PRIVATE METHODS.
			// ---


			// I transform the error response, unwrapping the application dta from
			// the API response payload.
			function handleError( response ) {

				// The API response from the server should be returned in a
				// nomralized format. However, if the request was not handled by the
				// server (or what not handles properly - ex. server error), then we
				// may have to normalize it on our end, as best we can.
				if (
					! angular.isObject( response.data ) ||
					! response.data.message
					) {

					return( $q.reject( "An unknown error occurred." ) );

				}

				// Otherwise, use expected error message.
				return( $q.reject( response.data.message ) );

			}


			// I transform the successful response, unwrapping the application data
			// from the API response payload.
			function handleSuccess( response ) {
//				$scope.commentList = response.data;
				return response.data;
			}

		}
	);
</script>