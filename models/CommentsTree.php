<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments_tree".
 *
 * @property string $id
 * @property string $parent_id
 * @property string $path
 */
class CommentsTree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'path'], 'required'],
            [['id', 'parent_id'], 'integer'],
            [['path'], 'string', 'max' => 20000],
            [['id', 'parent_id'], 'unique', 'targetAttribute' => ['id', 'parent_id'], 'message' => 'The combination of ID and Parent ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'path' => 'Path',
        ];
    }
}
