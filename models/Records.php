<?php

namespace app\models;

use Yii;
use app\components;

/**
 * This is the model class for table "records".
 *
 * @property string $id
 * @property string $owner_id
 * @property string $title
 * @property string $body
 */
class Records extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'title', 'body'], 'required'],
            [['owner_id'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'title' => 'Title',
            'body' => 'Body',
        ];
    }
	
	/**
	 * Generate new records
	 * 
	 * @param array $users
	 * @param int $num_records - amount of new records
	 */
	public static function generateNewRecords($users, $num_records)
	{
		$records = array();
		shuffle($users);
		while ($num_records > 0) {
			/**
			 * randomize amount of records for user
			 */
			$user_records = rand(0, $num_records);
			for ($i = 0; $i < $user_records; $i++) {
				$text = components\GenerateHelperComponent::text();
				$records[] = [
					current($users),
					substr($text, 0, 50).'...',
					$text
				];
			}
			/**
			 * If users ended, add them to last user
			 */
			if (next($users) === false) {
				for ($i = 0; $i < $num_records; $i++) {
					$text = components\GenerateHelperComponent::text();
					$records[] = [
						current($users),
						substr($text, 0, 50).'...',
						$text
					];
				}
				$num_records = 0;
			}
			$num_records = $num_records - $user_records;
		}
		static::getDb()->createCommand()->batchInsert('records', array('owner_id', 'title', 'body'), $records)->execute();
	}
	
	/**
	 * Get all records for users
	 * 
	 * @param array $users
	 * @return array
	 */
	public static function getAllNewRecords($users)
	{
		return static::getDb()->createCommand('SELECT id FROM records WHERE owner_id IN ('.implode(',', $users).')')->queryColumn();
	}
}
