<?php

namespace app\models;

use Yii;
use app\components;
use yii\db\Query;

/**
 * This is the model class for table "comments".
 *
 * @property string $id
 * @property string $owner_id
 * @property string $record_id
 * @property string $body
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'record_id', 'body'], 'required'],
            [['owner_id', 'record_id'], 'integer'],
            [['body'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'record_id' => 'Record ID',
            'body' => 'Body',
        ];
    }
	
	/**
	 * Generate new comments
	 * 
	 * @param array $users
	 * @param array $records
	 * @param iint $num_comments - amount of new comments
	 */
	public static function generateNewComments($users, $records, $num_comments, $deep)
	{
		$comments = array();
		shuffle($records);
		while ($num_comments > 0) {
			/**
			 * randomize amount of comments for record
			 */
			$record_comments = rand(0, $num_comments);
			if ($record_comments > 40000) {
				$record_comments = 40000;
			}
			for ($i = 1; $i <= $record_comments; $i++) {
				$text = components\GenerateHelperComponent::text(true);
				$comments[] = [
					$users[array_rand($users)],
					current($records),
					$text
				];
			}
			if ($comments) {
				static::getDb()->createCommand()->batchInsert('comments', array('owner_id', 'record_id', 'body'), $comments)->execute();
				$comments = array();
			}
			/**
			 * If records ended, add them to last record
			 */
			if (next($records) === false) {
				for ($i = 1; $i <= $num_comments; $i++) {
					$text = components\GenerateHelperComponent::text(true);
					$comments[] = [
						$users[array_rand($users)],
						current($records),
						$text
					];
					if (($i % 40000) == 0) {
						static::getDb()->createCommand()->batchInsert('comments', array('owner_id', 'record_id', 'body'), $comments)->execute();
						$comments = array();
					}
				}
				$num_comments = 0;
			}
			$num_comments = $num_comments - $record_comments;
		}
		if ($comments) {
			while ($rows = array_splice($comments, 0, 40000)) {
				static::getDb()->createCommand()->batchInsert('comments', array('owner_id', 'record_id', 'body'), $rows)->execute();
			}
		}
		unset($comments);
		
		self::buildTree($users, $deep);
	}

	/**
	 * Build tree of comments
	 * 
	 * @param array $users
	 */
	private static function buildTree($users, $deep)
	{
		$query = (new Query())
			->select('id, record_id')
			->from('comments')
			->where(['owner_id' => $users])
			->orderBy('record_id');

		$record_id = 0;
		$comments_tree = array();
		foreach ($query->each() as $row) {
			/**
			 * tree root
			 */
			if ($record_id != $row['record_id']) {
				$record_id = $row['record_id'];
				$parents = array(0);
				$step = 1;
				$parents[1] = $row['id'];
				if ($comments_tree) {
					while ($rows = array_splice($comments_tree, 0, 40000)) {
						static::getDb()->createCommand()->batchInsert('comments_tree', array('id', 'parent_id', 'path'), $rows)->execute();
					}
					$comments_tree = array();
				}
				$comments_tree[$row['id']] = [$row['id'], $parents[$step-1], $row['id']];
			}
			else {
				/**
				 * generate leaves
				 */
				if (rand(0, 1)) {
					if ($step < $deep) {
						$step++;
					}
					$parents[$step] = $row['id'];
				}
				else {
					if ($step > 1 && rand(0, 1)) {
						$step--;
					}
					$parents[$step] = $row['id'];
				}
				if ($parents[$step-1] == 0) {
					$comments_tree[$row['id']] = [$row['id'], $parents[$step-1], $row['id']];
				}
				else {
					$comments_tree[$row['id']] = [$row['id'], $parents[$step-1], $comments_tree[$parents[$step-1]][2].','.$row['id']];
				}
			}
		}
		if ($comments_tree) {
			while ($rows = array_splice($comments_tree, 0, 40000)) {
				static::getDb()->createCommand()->batchInsert('comments_tree', array('id', 'parent_id', 'path'), $rows)->execute();
			}
			unset($comments_tree);
		}
	}
}
