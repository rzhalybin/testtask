<?php

namespace app\models;

use Yii;
use app\components;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $login
 * @property string $name
 * @property integer $pre_save
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'name'], 'required'],
            [['pre_save'], 'integer'],
            [['login'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 22]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'name' => 'Name',
            'pre_save' => 'Pre Save',
        ];
    }
	
	/**
	 * Begin generate new users
	 * 
	 * @param int $num_users - amount of new users
	 */
	public static function preGenerateNewUsers($num_users)
	{
		$users = array();
		for ($i = 0; $i < $num_users; $i++) {
			$users[] = [
				components\GenerateHelperComponent::name(),
				components\GenerateHelperComponent::name(),
			];
		}
		static::getDb()->createCommand()->batchInsert('users', array('login', 'name'), $users)->execute();
	}
	
	/**
	 * Get all pre generated users
	 * 
	 * @return array
	 */
	public static function getAllNewUsers()
	{
		return static::getDb()->createCommand('SELECT id FROM users WHERE pre_save = 0')->queryColumn();
	}
	
	/**
	 * Commit new users
	 * 
	 * @param array $users
	 */
	public static function finishGenerateNewUsers($users)
	{
		static::getDb()->createCommand()->update('users', array('pre_save' => 1), 'id IN ('.implode(',', $users).')')->execute();
	}
}
