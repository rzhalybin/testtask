<?php

namespace app\components;

use yii\base\Component;

class GenerateHelperComponent
{
	private static $letters = [
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
	];
	
	private static $dictionary = [
		'while',
		'components',
		'are', 
		'very', 
		'powerful', 
		'they', 
		'are', 
		'a', 
		'bit', 
		'heavier', 
		'than', 
		'normal', 
		'objects,',
		'due', 
		'to', 
		'the', 
		'fact', 
		'that', 
		'it', 
		'takes', 
		'extra', 
		'memory', 
		'and', 
		'CPU', 
		'time', 
		'to', 
		'support', 
		'event', 
		'and', 
		'behavior', 
		'functionality', 
		'in', 
		'particular', 
		'if', 
		'your', 
		'components', 
		'do', 
		'not', 
		'need', 
		'these', 
		'two', 
		'features', 
		'you', 
		'may', 
		'consider', 
		'extending', 
		'your', 
		'component', 
		'class', 
		'from', 
		'yii\base\Object', 
		'instead', 
		'of', 
		'yii\base\Component', 
		'doing', 
		'so', 
		'will', 
		'make', 
		'your', 
		'components',
		'as',
		'efficient',
		'as',
		'normal',
		'PHP',
		'objects',
		'but',
		'with',
		'added',
		'support',
		'for',
		'properties'
	];

	/**
	 * Generate text for record/comment
	 * 
	 * @return sttring
	 */
	public static function text($short = false)
	{
		$i = 0;
		$min = 30;
		$max = 72;
		if ($short) {
			$min = 3;
			$max = 12;
		}
		$dictionary = array();
		/**
		 * Generate text
		 */
		while ($i < rand(1, 3)) {
			shuffle(self::$dictionary);
			$dictionary = array_merge($dictionary, array_slice(self::$dictionary, 0, rand($min, $max)));
			$i++;
		}
		
		/**
		 * Insert full stops
		 */
		$dictionary_len = count($dictionary);
		$fullstop_num = rand(0, 5);
		
		for ($i = 0; $i < $fullstop_num; $i++) {
			$fullstop_pos = rand(2, $dictionary_len-1);
			if (strpos($dictionary[$fullstop_pos], '.') === false) {
				$dictionary[$fullstop_pos] .= '.';
				if (isset($dictionary[$fullstop_pos+1])) {
					$dictionary[$fullstop_pos+1] = ucfirst($dictionary[$fullstop_pos+1]);
				}
			}
		}
		
		$text = implode(' ', $dictionary);
		
		return $text;
	}
	
	/**
	 * Generate name/login for user
	 * 
	 * @return sttring
	 */
	public static function name()
	{
		shuffle(self::$letters);
		return implode('', array_slice(self::$letters, 0, rand(4, 20)));
	}
	
	/**
	 * 
	 * @param string $val
	 * @return int
	 */
	public static function return_bytes($val) 
	{
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last) {
			// Модификатор 'G' доступен, начиная с PHP 5.1.0
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}

		return $val;
	}
	
}

